<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Detalle de producto</h1>

        <ul>
            <li>Nombre: <?php echo $product->name ?></li>
            <li>Precio: <?php echo $product->price ?></li>
            <li>Tipo: <?php echo $product->type_id ?></li>
        </ul>
    </div>
    <a href="/product">Volver</a>
  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
