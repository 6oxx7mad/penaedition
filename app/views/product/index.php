<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Lista de Productos</h1>

    <table class="table table-striped">
      <thead>
        <tr>
          <th>Id</th>
          <th>Nombre</th>
          <th>Precio</th>
          <th>Operaciones</th>    
        </tr>
      </thead>
      <tbody>
        <?php foreach ($todos as $product): ?>
          <tr>
          <td><?php echo $product->id ?></td>
          <td><?php echo $product->name ?></td>
          <td><?php echo $product->price ?></td>
          <td><?php echo $product->type_id ?></td>    
          <td>
            <a class="btn btn-primary" href="/product/show/<?php echo $product->id ?>">Ver</a>
            <a class="btn btn-primary" href="/product/delete/<?php echo $product->id ?>">Borrar</a>
            <a class="btn btn-primary" href="/product/edit/<?php echo $product->id ?>">Editar</a>
            <a class="btn btn-primary" href="/basket/add/<?php echo $product->id ?>">Añadir</a>
          </td>
          </tr>
        <?php endforeach ?>
      </tbody>
    </table>
    <?php for ($i=1; $i <= $pages; $i++) { ?>
      <?php if ($i != $page): ?>
      <a href="/product?page=<?php echo $i ?>" class="btn">
        <?php echo $i ?>
      </a>
      <?php else: ?>
        <span class="btn">
        <?php echo $i ?>
        </span>
      <?php endif ?>
    <?php } ?>
    <hr>

    <a href="/product/create">Nuevo </a>
    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
