<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Alta de producto</h1>

      <form method="post" action="/product/store">

        <div class="form-group">
          <label>Nombre</label>
          <input type="text" name="name" class="form-control">
        </div>
        <div class="form-group">
          <label>precio</label>
          <input type="text" name="precio" class="form-control">
        </div>         
        <div class="form-group">
            <label>Tipos</label>
            <select name="tipo">
                <?php foreach($productsTypes as $tipo): ?>
                    <option name="<?php echo $tipo->id; ?>" value="<?php echo $tipo->id; ?>">
                        <?php echo $tipo->name; ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>
        <button type="submit" class="btn btn-default">Enviar</button>
      </form>
    </div>
  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
