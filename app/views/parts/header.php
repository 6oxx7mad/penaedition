<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Peña Pena</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/home">Home</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/user">Usuarios</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/product">Productos</a>
            </li>
        </ul>

        <ul class="navbar-nav">
            <li>
            <?php if(isset($_SESSION['penista'])){
                echo '<a class="nav-link" href="/user/useri">' . $_SESSION['penista']->name . '</a>';    
            }        
            ?>
            </li>
            <li class="nav-item active">  
            <?php if(isset($_SESSION['logeado']) && $_SESSION['logeado'] == 'si'){
                echo '<a class="nav-link" href="/login/logout">Logout</a>';    
            }else{
                echo '<a class="nav-link" href="/login">Login</a>';    
            }        
            ?>
            </li>
        </ul>
    </div>
</nav>
