<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Login</h1>
      <form method="post" action="/login/login">

        <div class="form-group">
          <label>Correo</label>
          <input type="text" name="correo" class="form-control">
        </div>
        <div class="form-group">
          <label>Contraseña</label>
          <input type="password" name="clave" class="form-control">
        </div>
        <button type="submit" class="btn btn-default">Enviar</button>
      </form>
        <?php if(isset($_SESSION['error']) && $_SESSION['error'] == "Datos no válidos"){
                echo "<h2>" . $_SESSION['error'] . "</h2>";
            }
        ?>    
    </div>
      
  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
