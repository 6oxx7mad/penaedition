<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Lista de Tipo producto</h1>

    <table class="table table-striped">
      <thead>
        <tr>
          <th>Id</th>
          <th>Nombre</th>
          <th>Operaciones</th>    
        </tr>
      </thead>
      <tbody>
        <?php foreach ($todos as $product): ?>
          <tr>
          <td><?php echo $product->id ?></td>
          <td><?php echo $product->name ?></td>
          <td>
              <a class="btn btn-primary" href="/type/show/<?php echo $product->id ?>">Ver</a>
          </td
          </tr>
        <?php endforeach ?>
      </tbody>
    </table>
    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
