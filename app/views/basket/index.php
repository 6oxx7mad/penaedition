<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Cesta</h1>

    <table class="table table-striped">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Cantidad</th>
          <th>Operaciones</th>    
        </tr>
      </thead>
      <tbody>
        <?php foreach ($_SESSION['cesta'] as $item): ?>
          <tr>
          <td><?php echo $item->nombre ?></td>
          <td><?php echo $item->contador ?></td>
          <td>
            <a class="btn btn-primary" href="/basket/up/<?php echo $item->nombre ?>">Agregar</a>
            <a class="btn btn-primary" href="/basket/down/<?php echo $item->nombre ?>">Restar</a>
            <a class="btn btn-primary" href="/basket/remove/<?php echo $item->nombre ?>">Quitar</a>
          </td>
          </tr>
        <?php endforeach ?>
      </tbody>
    </table>
    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
