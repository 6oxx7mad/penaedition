<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Lista de Penistas</h1>

    <table class="table table-striped">
      <thead>
        <tr>
          <th>Id</th>
          <th>Nombre</th>
          <th>Apellidos</th>
          <th>Edad</th>
          <th>Email</th>
          <th>admin</th>
          <th>active</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($users as $user): ?>
          <tr>
          <td><?php echo $user->id ?></td>
          <td><?php echo $user->name ?></td>
          <td><?php echo $user->surname ?></td>
          <td><?php echo $user->birthdate ?></td>
          <td><?php echo $user->email ?></td>
          <td><?php echo $user->admin ?></td>
          <td><?php echo $user->active ?></td>
          <td>
            <a class="btn btn-primary" href="/user/show/<?php echo $user->id ?>">Ver</a>
            <a class="btn btn-primary" href="/user/delete/<?php echo $user->id ?>">Borrar</a>
            <a class="btn btn-primary" href="/user/edit/<?php echo $user->id ?>">Editar</a>
          </td>
          </tr>
        <?php endforeach ?>
      </tbody>
    </table>
    <?php for ($i=1; $i <= $pages; $i++) { ?>
      <?php if ($i != $page): ?>
      <a href="/user?page=<?php echo $i ?>" class="btn">
        <?php echo $i ?>
      </a>
      <?php else: ?>
        <span class="btn">
        <?php echo $i ?>
        </span>
      <?php endif ?>
    <?php } ?>
    <hr>

    <a href="/register/">Nuevo </a>
    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
