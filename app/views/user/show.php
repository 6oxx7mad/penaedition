<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Detalle de usuario</h1>

        <ul>
            <li>Nombre: <?php echo $user->name ?></li>
            <li>Apellido: <?php echo $user->surname ?></li>
            <li>Edad: <?php echo $user->birthdate ?></li>
            <li>email: <?php echo $user->email ?></li>
            <li>admin: <?php echo $user->admin ?></li>
            <li>active: <?php echo $user->active ?></li>
        </ul>
    </div>
    <a href="/user">Volver</a>
  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
