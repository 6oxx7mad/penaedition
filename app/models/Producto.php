<?php
namespace App\models;

use PDO;
use Core\Model;

require_once '../core/Model.php';

class Producto extends Model{

  function __construct() {}

    public static function all()
    {
        $db = Producto::db();
        $statement = $db->query('SELECT * FROM products');
        $product = $statement->fetchAll(PDO::FETCH_CLASS, Producto::class);
        return $product;
    }
    
    public function paginate($size = 10)
    {
        if (isset($_REQUEST['page'])) {
            $page = (integer) $_REQUEST['page'];
        } else {
            $page = 1;
        }

        $offset = ($page - 1) * $size;

        $db = Producto::db();
        $statement = $db->prepare('SELECT * FROM products LIMIT :pagesize OFFSET :offset');
        $statement->bindValue(':pagesize', $size, PDO::PARAM_INT);
        $statement->bindValue(':offset', $offset, PDO::PARAM_INT);
        $statement->execute();

        $products = $statement->fetchAll(PDO::FETCH_CLASS, Producto::class);

        return $products;
    }
    
    public static function rowCount()
    {
        $db = Producto::db();
        $statement = $db->prepare('SELECT count(id) as count FROM products');
        $statement->execute();

        $rowCount = $statement->fetch(PDO::FETCH_ASSOC);
        return $rowCount['count'];
    }

    public static function find($id)
    {
        $db = Producto::db();
        $statemet = $db->prepare('SELECT * FROM products WHERE id=?');
        $statemet->bindValue(1, $id, PDO::PARAM_INT);
        $statemet->execute();
        $statemet-> setFetchMode(PDO::FETCH_CLASS,Producto::class);
        $product = $statemet->fetchAll(PDO::FETCH_CLASS);

        return $product[0];
    }

    public function insert()
    {
        $db = Producto::db();
        $statemet = $db->prepare('INSERT INTO products(name, price, type_id) VALUES( :name, :price, :type_id)');
        $statemet->bindValue(':name', $this->name);
        $statemet->bindValue(':price', $this->price);
        $statemet->bindValue(':type_id', $this->type_id);

        return $statemet->execute();
    }


    public function delete($id)
    {
        $db = Producto::db();
        $statemet = $db->prepare('DELETE FROM products WHERE id= :id');
        $statemet->bindValue(':id',$id);

        return $statemet->execute();
    }

    public function save()
    {
        $db = Producto::db();
        $statemet = $db->prepare('UPDATE products SET  name=:name,price=:price,type_id=:type_id WHERE id=:id ;');
        $statemet->bindValue(':id', $this->id);
        $statemet->bindValue(':name', $this->name);
        $statemet->bindValue(':price', $this->price);
        $statemet->bindValue(':type_id', $this->type_id);

        return $statemet->execute();
    }

}
?>
