<?php
/**
*
*/
namespace App\Models;

use PDO;
use Core\Model;
use App\Models\Producto;

require_once '../core/Model.php';
require_once '../app/models/Producto.php';
/**
*
*/
class ProductType extends Model
{

    function __construct(){}

    public function __get($atributoDesconocido)
    {
        if (method_exists($this, $atributoDesconocido)) {
            $this->$atributoDesconocido = $this->$atributoDesconocido();
            return $this->$atributoDesconocido;
        } else {
            return "";
        }
    }

    public function products($id)
    {
        $db = Producto::db();
        $statement = $db->prepare('SELECT * FROM products WHERE type_id = :id');
        $statement->bindValue(':id', $id);
        $statement->execute();
        $products = $statement->fetchAll(PDO::FETCH_CLASS, Producto::class);
        
        return $products;
    }

    public function find($id)
    {
        $db = ProductType::db();
        $stmt = $db->prepare('SELECT * FROM product_types WHERE id=:id');
        $stmt->execute(array(':id' => $id));
        $stmt->setFetchMode(PDO::FETCH_CLASS, ProductType::class);
        $productType = $stmt->fetch(PDO::FETCH_CLASS);

        return $productType;
    }
    
    public function findName($id)
    {
        $db = ProductType::db();
        $stmt = $db->prepare('SELECT name FROM product_types WHERE id=:id');
        $stmt->execute(array(':id' => $id));
        $stmt->setFetchMode(PDO::FETCH_CLASS, ProductType::class);
        $nombre = $stmt->fetch(PDO::FETCH_CLASS);

        return $nombre->name;
    }
    
    public static function all(){
        $db = ProductType::db();
        $statement = $db->query('SELECT * FROM product_types');
        $tipo = $statement->fetchAll(PDO::FETCH_CLASS, ProductType::class);

        return $tipo;
    }


}
