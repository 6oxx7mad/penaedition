<?php
namespace App\Models;

use PDO;
use Core\Model;

require_once '../core/Model.php';
/**
*
*/
class Basket extends Model
{
    public $nombre;
    public $contador;
    
    function __construct(){}
    
    public function __set($var, $val)
    {
        trigger_error("Property $var doesn't exists and cannot be set.", E_USER_ERROR);
    }

    public function  __get($var)
    {
        return $this->$var;
    }
}
