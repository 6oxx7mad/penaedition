<?php
namespace App\Controllers;

use \App\Models\User;
require_once '../app/models/User.php';

class LoginController
{

    function __construct() {}

    public function index()
    {
        require "../app/views/login.php";
    }
    
    public function login()
    {
        $correo = $_REQUEST['correo'];
        $clave = $_REQUEST['clave'];
        if($correo != null && $clave != null){
            unset($_SESSION['error']);
            $_SESSION['logeado'] = "si";
            $user = User::findCorreo($correo);
            if($user == false){
                $_SESSION['error'] = "Datos no válidos";
                header('Location:/login');
            }else {
                if($user->passwordVerify($clave)){
                    $_SESSION['penista'] = $user;
                    header('Location:/user/useri');    
                }else{
                    $_SESSION['error'] = "Datos no válidos";
                    header('Location:/login');
                }
            }            
        }else{
            $_SESSION['error'] = "Datos no válidos";
            header('Location:/login');
        }
    }
    
    public function logout()
    {
        unset($_SESSION['logeado']);
        unset($_SESSION['penista']);
        unset($_SESSION['cesta']);
        unset($_SESSION['error']);
        //require "../app/views/login.php";
        header('Location:/login');
    }
}
