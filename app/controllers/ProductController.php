<?php

namespace App\Controllers;

use \app\models\Producto;
use \App\Models\ProductType;
require_once '../app/models/producto.php';
require_once '../app/models/ProductType.php';

class ProductController
{

    function __construct(){}

    public function index()
    {
        $pagesize = 4;
        $todos = Producto::all();
        $products = Producto::paginate($pagesize);
        $rowCount = Producto::rowCount();

        $pages = ceil($rowCount / $pagesize);
        if (isset($_REQUEST['page'])) {
            $page = (integer) $_REQUEST['page'];
        } else {
            $page = 1;
        }

        foreach($todos as $producto)
        {
            $producto->type_id = ProductType::findName($producto->type_id);        
        }
        
        require "../app/views/product/index.php";
    }

    public function show($arguments)
    {
        $id = (int) $arguments[0];
        $product = Producto::find($id);
        $product->type_id = ProductType::findName($product->type_id);        
        
        require "../app/views/product/show.php";
    }

    public function create()
    {
        $productsTypes=ProductType::all();
        require '../app/views/product/create.php';
    }

    public function store()
    {
        $product = new Producto();
        $product->name = $_REQUEST['name'];
        $product->price = $_REQUEST['precio'];
        $product->type_id = $_REQUEST['tipo'];
        $product->insert();
        header('Location:/product');
    }

    public function edit($arguments)
    {
        $id = (int)$arguments[0];
        $product=Producto::find($id);
        $productsTypes=ProductType::all();
        require "../app/views/product/edit.php";
    }

    public function update()
    {
        $product = new Producto();
        $product->id = $_REQUEST['id'];
        $product->name = $_REQUEST['name'];
        $product->price = $_REQUEST['precio'];
        $product->type_id = $_REQUEST['tipo'];
        $product->save();
        header('Location:/product');
    }

    public function delete($arguments)
    {
        $id = (int)$arguments[0];
        Producto::delete($id);
        header('Location:/product');
    }
}
?>
