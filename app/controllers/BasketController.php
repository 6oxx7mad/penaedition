<?php

namespace App\Controllers;

use \app\models\Producto;
use \app\models\Basket;

require_once '../app/models/producto.php';
require_once '../app/models/basket.php';

class BasketController
{
    
    function __construct(){}

    public function index()
    {        
        require "../app/views/basket/index.php";
    }
    
    public function add($arguments)
    {
        $id = (int) $arguments[0];
        $product = Producto::find($id);
        $comprado = new Basket();
        $comprado->nombre = str_replace(' ', '', $product->name);
        $contador = ucwords($product->name) . "Cont";    
        if (!isset($_SESSION['cesta'])){
            $comprado->contador = 1;
            $_SESSION['cesta'][$comprado->nombre] = $comprado;
        }else{
            foreach($_SESSION['cesta'] as $item)
            {
                if($item->nombre == $comprado->nombre){
                    $_SESSION['cesta'][$comprado->nombre]->contador++;
                }else{
                    $comprado->nombre = str_replace(' ', '', $product->name);
                    $comprado->contador = 1;
                    $_SESSION['cesta'][$comprado->nombre] = $comprado;    
                }
            }
        }
        require "../app/views/basket/index.php";
    }
    
    public function up($arguments)
    {
        $_SESSION['cesta'][$arguments[0]]->contador++;
        require "../app/views/basket/index.php";
    }
    
    public function down($arguments)
    {
        if($_SESSION['cesta'][$arguments[0]]->contador == 0){
           unset($_SESSION['cesta'][$arguments[0]]); 
        }else{
            $_SESSION['cesta'][$arguments[0]]->contador--;   
        }
        require "../app/views/basket/index.php";
    }
    
    public function remove($arguments)
    {
        unset($_SESSION['cesta'][$arguments[0]]);
        require "../app/views/basket/index.php";
    }

}
?>
