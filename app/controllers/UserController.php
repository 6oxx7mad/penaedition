<?php
namespace App\Controllers;

use \App\Models\User;
require_once '../app/models/User.php';

class UserController
{

    function __construct(){}

    public function index()
    {
        $pagesize = 4;
        $users = User::paginate($pagesize);
        $rowCount = User::rowCount();

        $pages = ceil($rowCount / $pagesize);
        if (isset($_REQUEST['page'])) {
            $page = (integer) $_REQUEST['page'];
        } else {
            $page = 1;
        }

        require "../app/views/user/index.php";
    }
    
    public function useri()
    {
        require "../app/views/useri.php";
    }

    public function show($arguments)
    {
        $id = (int) $arguments[0];
        $user = User::find($id);
        require "../app/views/user/show.php";
    }

    public function create()
    {
        require '../app/views/user/create.php';
    }

    public function store()
    {
        $user = new User();
        $user->name = $_REQUEST['name'];
        $user->surname = $_REQUEST['surname'];
        $user->birthdate = $_REQUEST['birthdate'];
        $user->email = $_REQUEST['email'];
        $id = $user->insert();
        $userP = User::find($id);
        $userP->setPassword($_REQUEST['pass']);
        header('Location:/user');
    }

    public function delete($arguments)
    {
        $id = (int) $arguments[0];
        $user = User::find($id);
        $user->delete();
        header('Location:/user');
    }

    public function edit($arguments)
    {
        $id = (int) $arguments[0];
        $user = User::find($id);
        require '../app/views/user/edit.php';
    }

    public function update()
    {
        $id = $_REQUEST['id'];
        $user = User::find($id);
        $user->name = $_REQUEST['name'];
        $user->surname = $_REQUEST['surname'];
        $user->birthdate = $_REQUEST['birthdate'];
        $user->email = $_REQUEST['email'];
        $user->save();
        header('Location:/user');
    }
}
