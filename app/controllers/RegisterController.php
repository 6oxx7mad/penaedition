<?php
namespace App\Controllers;

use \App\Models\User;
require_once '../app/models/User.php';
class RegisterController
{
    function __construct(){}

    public function index()
    {
        require '../app/views/register/register.php';
    }

    public function register()
    {
        $user = new User();
        $user->name = $_REQUEST['name'];
        $user->surname = $_REQUEST['surname'];
        $user->birthdate = $_REQUEST['birthdate'];
        $user->email = $_REQUEST['email'];
        //$user->password = $_REQUEST['password'];
        /*var_dump($_REQUEST['password']);
        exit();*/
        $id = $user->insert();
        $user->id = $id;
        $user->setPassword($_REQUEST['password']);
        header('Location:/user');
    }
}
