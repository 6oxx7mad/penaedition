<?php

namespace App\Controllers;

use \App\Models\producto;
use \App\Models\ProductType;
require_once '../app/models/producto.php';
require_once '../app/models/ProductType.php';

class TypeController
{

    function __construct(){}

    public function index()
    {
        $todos = ProductType::all();
        require "../app/views/productype/index.php";
    }

    public function show($arguments)
    {
        $id = (int) $arguments[0];
        $tipo = ProductType::find($id);
        $productosTipo = ProductType::products($id);
        require "../app/views/productype/show.php";
    }
    
}
