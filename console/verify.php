<?php
$password = $argv[1];
$id =  $argv[2];
echo $password . "\n";
echo "Id: $id\n";

require "../app/models/User.php";

$user = \App\Models\User::find($id);

echo "Usuario $user->name\n";

echo "La contraseña es ";
echo $user->passwordVerify($password) ? "valida" : "falsa";
echo "\n";

// session_start();
if ($user->passwordVerify($password)) {
    $_SESSION['user'] = $user;
} else {
    unset($_SESSION['user']);
}

echo "Datos de sesión:\n";
var_dump($_SESSION);


